// Background animations
var button = document.getElementsByClassName('button');
var transform = document.getElementsByClassName('transform');
var nadoki = ['n', 'a', 'd', 'o', 'k', 'i'];

for (var i = 0; i < button.length; i++) {
  button[i] = new addButtons(i);
}

// Add the classes of the selected button
function addClasses(classIndex){
  for (var i = 0; i < button.length; i++) {
    transform[i].classList.toggle('transform-' + nadoki[i] + '-active-' + classIndex);
  }
}

// Remove all other classes
function removeClasses(classIndex){
  var index = [1, 2, 3, 4, 5, 6];
  delete index[classIndex - 1];
  for (var i = 0; i < button.length; i++) {
      for (var d = 0; d < button.length; d++) {
          transform[d].classList.remove('transform-' + nadoki[d] + '-active-' + index[i]);
      }
    }
}

// Add addEventListeners to every button
function addButtons(i){
  button[i].addEventListener("click", function() {
      var classIndex = [i+1];
      addClasses(classIndex);
      removeClasses(classIndex);
  });
}

// Background warping effect
var _slicedToArray = function () {
	function sliceIterator(arr, i) {
		var _arr = [];
		var _n = true;
		var _d = false;
    _e = undefined;
		try {
      for (_i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true){
        _arr.push(_s.value);
        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;_e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  } return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if (Symbol.iterator in Object(arr)) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function createWarps(letter) {
  var svg = document.getElementById('svg-element-' + nadoki[letter]);
  var warp = new Warp(svg);
  var offset = 0;

  warp.interpolate(4);
  warp.transform(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),x = _ref2[0],y = _ref2[1];
    return [x, y, y];
  });

  function animate() {
  	warp.transform(function (_ref3) {
      var _ref4 = _slicedToArray(_ref3, 3),x = _ref4[0],y = _ref4[1],oy = _ref4[2];
      return [x, oy + 4 * Math.sin(x / 16 + offset), oy];
    });
    offset += 0.1;
  	requestAnimationFrame(animate);
  }

  animate();
}

for (var i = 0; i < button.length; i++) {
  createWarps(i);
}
