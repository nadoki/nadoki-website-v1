# Nadoki Website [![GitLab License](https://img.shields.io/badge/license-MIT-brightgreen)](https://gitlab.com/nadoki/nadoki-website-v1/blob/master/LICENSE)

## Table of contents

- [General info](#general-info)
- [Technologies](#technologies)
- [Setup](#setup)
- [Contributing](#contributing)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## General info

Nadoki is a mixing and mastering studio located at [Riverside Studios](https://riversidestudios.de/), a unique collective of renowned music artists and professionals in the heart of Berlin.

This is the repository of the first version of it's website. Check how it looked [here](https://nadoki.gitlab.io/nadoki-website-v1/).

The current version's repository is available [here](https://gitlab.com/nadoki/nadoki-website-v2). You can check the live version of the website at [nadoki.com](https://www.nadoki.com/) and get to know a bit more about the studio and it's members.

## Technologies

This website was created with HTML, CSS (SCSS) and JavaScript.

## Setup

This is aready the compiled version of the website.

To run this project, start by cloning it and then create a command-line http server using [Node.js](https://nodejs.org/en/):

```
cd [project folder path]
npx http-server
```

...this will start a local server on [localhost:8000](http://127.0.0.1:8080/).

## Contributing

This project is closed.

## Authors and acknowledgment

Designed and developed by [João Rodrigues](@joaocdvr).
